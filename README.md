# GoDutch - APP
#### GoDutch application frontend.

### RUN TARGETS

To run the app in localhost:4020, execute the following Make target:

```make run```

There is another target to run the app in a Docker container:

```make start```

### CI / CD

Pushes to Master branch will deploy the application in Vercel:

https://godutch.vercel.app/