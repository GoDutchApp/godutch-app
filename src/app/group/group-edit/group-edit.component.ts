import { GroupService } from './../../core/services/group.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Group } from 'src/app/core/models/Group';

@Component({
  selector: 'app-group-edit',
  templateUrl: './group-edit.component.html',
  styleUrls: ['./group-edit.component.scss'],
})
export class GroupEditComponent implements OnInit {
  id: string;
  referral: string;
  group: Group;
  error: string = '';

  constructor(
    private route: ActivatedRoute,
    private groupService: GroupService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.id = params['id'];
      this.getGroup();
    });
    this.route.queryParams.subscribe((params) => {
      this.referral = params.referral;
    });
  }

  getGroup(): void {
    this.groupService.findById(this.id).subscribe((group) => {
      this.group = group;
    });
  }

  onSubmit(): void {
    this.groupService.update({ ...this.group }).subscribe(
      () => {
        this.router.navigate([`/groups/${this.id}`], {
          queryParams: { referral: this.referral },
        });
      },
      (err) => {
        this.error =
          err.errors && err.errors.length ? err.errors[0] : err.message;
      }
    );
  }
}
