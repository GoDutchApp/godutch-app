import { Balance } from './../../core/models/Balance';
import { Expense } from './../../core/models/Expense';
import { ExpenseService } from './../../core/services/expense.service';
import { GroupService } from './../../core/services/group.service';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Group } from 'src/app/core/models/Group';

@Component({
  selector: 'app-group-detail',
  templateUrl: './group-detail.component.html',
  styleUrls: ['./group-detail.component.scss'],
})
export class GroupDetailComponent implements OnInit {
  id: string;
  referral: string;
  group: Group;
  expenses: Expense[];
  balance: Balance;

  constructor(
    private route: ActivatedRoute,
    private groupService: GroupService,
    private expenseService: ExpenseService
  ) {}

  ngOnInit(): void {
    this.route.params.subscribe((params) => {
      this.id = params['id'];
      this.getGroup();
    });
    this.route.queryParams.subscribe((params) => {
      this.referral = params.referral;
    });
  }

  getGroup(): void {
    this.groupService.findById(this.id).subscribe((group) => {
      this.group = group;
      this.getExpenses();
      this.getBalance();
    });
  }

  getExpenses(): void {
    this.expenseService.findByGroup(this.group.id).subscribe((expenses) => {
      this.expenses = expenses;
    });
  }

  getBalance(): void {
    this.expenseService.calculateBalance(this.group.id).subscribe((balance) => {
      this.balance = balance;
    });
  }
}
