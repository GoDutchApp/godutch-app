import { GroupService } from './../../core/services/group.service';
import { UserService } from './../../core/services/user.service';
import { ExpenseService } from './../../core/services/expense.service';
import { Expense } from './../../core/models/Expense';
import { Component, OnInit } from '@angular/core';
import * as uuid from 'uuid';
import { Router, ActivatedRoute } from '@angular/router';
import { Group } from 'src/app/core/models/Group';

@Component({
  selector: 'app-expense-create',
  templateUrl: './expense-create.component.html',
  styleUrls: ['./expense-create.component.scss'],
})
export class ExpenseCreateComponent implements OnInit {
  expense: Expense = new Expense();
  groupId: string;
  userEmail: string;
  error: string = '';

  constructor(
    private route: ActivatedRoute,
    private expenseService: ExpenseService,
    private groupService: GroupService,
    private userService: UserService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.route.queryParams.subscribe((params) => {
      this.groupId = params.group;
      this.getGroup();
    });
  }

  getGroup(): void {
    this.groupService.findById(this.groupId).subscribe((group) => {
      this.expense = {
        ...this.expense,
        group,
      };
    });
  }

  onSubmit(): void {
    this.userService.findByEmail(this.userEmail).subscribe(
      (paidBy) => {
        const id = uuid.v4();
        this.expenseService
          .create({ ...this.expense, id, paidBy })
          .subscribe(() => {
            this.router.navigate([`/groups/${this.expense.group.id}`]);
          });
      },
      (err) => {
        this.error =
          err.errors && err.errors.length ? err.errors[0] : err.message;
      }
    );
  }
}
