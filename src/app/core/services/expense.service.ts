import { Balance } from './../models/Balance';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

import { ApiService } from './api.service';
import { Expense } from '../models/Expense';
import { map } from 'rxjs/operators';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root',
})
export class ExpenseService {
  constructor(private apiService: ApiService) {}

  private EXPENSES_PATH: String = 'expenses';
  private GROUP_PATH: String = 'group';

  findById(id: string): Observable<Expense> {
    return this.apiService.get(`${this.EXPENSES_PATH}/${id}`);
  }

  findByGroup(groupId: string): Observable<Expense[]> {
    return this.apiService
      .get(`${this.EXPENSES_PATH}/${this.GROUP_PATH}/${groupId}`)
      .pipe(
        map((expenses) =>
          expenses.sort((a, b) => moment(b.paidAt).diff(moment(a.paidAt)))
        )
      );
  }

  calculateBalance(groupId: string): Observable<Balance> {
    return this.apiService.get(
      `${this.EXPENSES_PATH}/${this.GROUP_PATH}/balance/${groupId}`
    );
  }

  create(expense: Expense): Observable<void> {
    return this.apiService.put(`${this.EXPENSES_PATH}`, {
      ...expense,
      groupId: expense.group.id,
      paidBy: expense.paidBy.id,
    });
  }

  update(expense: Expense): Observable<void> {
    return this.apiService.patch(`${this.EXPENSES_PATH}`, {
      ...expense,
      groupId: expense.group.id,
      paidBy: expense.paidBy.id,
    });
  }

  delete(id: string): Observable<void> {
    return this.apiService.delete(`${this.EXPENSES_PATH}/${id}`);
  }
}
